# 3GPP citation generator for Bibtex

```bash
docker build . -t woranhun/3gpp-citations
docker run --rm -it -v $(pwd):/data:z woranhun/3gpp-citations
```

Where pwd contains a bib folder, inside the bib, there is an exported.xlsx. The output will be bib/3gpp.bib
