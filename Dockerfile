FROM ubuntu:18.04

RUN apt update && export DEBIAN_FRONTEND=noninteractive \
    && apt install -yq \
    python3-pip libxml2-dev libxslt-dev python-dev \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install 3gpp-citations && pip3 install 3gpp-citations[test]

WORKDIR /tmp
CMD ["3gpp-citations","--xelatex", "-i", "$0", "-o", "$1"]